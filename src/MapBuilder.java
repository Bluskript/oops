import StdLib.StdAudio;
import game.Beat;
import game.Map;

import javax.imageio.ImageIO;
import java.io.File;
import java.util.ArrayList;

// This generates maps given times for beats
public class MapBuilder {
    private File imageFile;
    private File soundFile;
    private ArrayList<Beat> beats;
    private String mapArtist;
    private String mapName;

    MapBuilder(File imageFile, File soundFile) {
        this.imageFile = imageFile;
        this.soundFile = soundFile;
        this.beats = new ArrayList<>();
    }

    void addBeat(Beat b) {
        beats.add(b);
    }

    void setMapArtist(String newArtist) {
        this.mapArtist = newArtist;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    Map makeMap() throws Exception {
        Map m = new Map();
        m.setImage(ImageIO.read(imageFile));
        m.setSong(StdAudio.read(soundFile.getAbsolutePath()));
        m.setBeats(beats);
        m.setMapArtist(this.mapArtist);
        m.setMapName(this.mapName);
        return m;
    }
}
