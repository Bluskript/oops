import StdLib.StdDraw;
import game.Menu;

enum GameScreen {
    MENU,
    GAME
}

public class Game {
    private Menu menu;
    private GameScreen screen;
    private boolean rightArrow;
    private boolean leftArrow;

    Game() {
        menu = new Menu("./maps");
        menu.readMaps();
        screen = GameScreen.MENU;
        menu.draw(screen);
        StdDraw.show();
    }

    void draw() {
    }

    void mouseHandler() {

    }

    void rightArrow() {
        if(!rightArrow) {
            rightArrow = true;
            menu.rightArrow();
        }
    }

    void rightArrowUp() {
        rightArrow = false;
    }

    void leftArrow() {
        if(!leftArrow) {
            leftArrow = true;
            menu.leftArrow();
        }
    }

    void leftArrowUp() {
        leftArrow = false;
    }

    public Menu getMenu() {
        return menu;
    }
}
