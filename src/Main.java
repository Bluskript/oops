import StdLib.StdDraw;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class Main {
    static boolean MAKING_MAP = false;

    public static void main(String[] args) {
        if(MAKING_MAP) {
            MapBuilder builder = new MapBuilder(new File("bg.jpg"), new File("tower of heaven.wav"));
            builder.setMapArtist("Feint");
            builder.setMapName("Tower Of Heaven");
            try {
                FileOutputStream fOut = new FileOutputStream("tower of heaven.oopsmap");
                ObjectOutputStream oOut = new ObjectOutputStream(fOut);
                oOut.writeObject(builder.makeMap());
                oOut.close();
                System.out.println("Oops game.Map Generated");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            StdDraw.setCanvasSize(1024, 576);
            StdDraw.setXscale(0, 1920);
            StdDraw.setYscale(0, 1080);
            StdDraw.enableDoubleBuffering();
            StdDraw.setFont(FontManager.standard);
            Game g = new Game();
            while (true) {
                g.draw();
                if(StdDraw.mousePressed()) {
                    g.mouseHandler();
                }
                if(StdDraw.isKeyPressed(KeyEvent.VK_RIGHT)) {
                    g.rightArrow();
                }
                else {
                    g.rightArrowUp();
                }
                if(StdDraw.isKeyPressed(KeyEvent.VK_LEFT)) {
                    g.leftArrow();
                }
                else {
                    g.leftArrowUp();
                }
            }
        }
    }
}
