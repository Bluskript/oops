package game;

import StdLib.StdDraw;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Menu {
    private File mapFolder;
    private final File[] mapFiles;
    private final ArrayList<Map> maps;
    private final AudioPlayer player;
    private int selectedMap = 0;

    Menu(String mapFolder) {
        this.mapFolder = new File(mapFolder);
        this.maps = new ArrayList<>();
        this.player = new AudioPlayer();
        this.mapFiles = this.mapFolder.listFiles((__, name) -> name.toLowerCase().endsWith(".oopsmap"));
        new Thread(player).start();
    }

    void readMaps() {
        if (mapFiles == null) {
            StdDraw.setPenColor(0, 0, 0);
            StdDraw.text(1024, 576, "No maps found");
            return;
        }
        Arrays.stream(mapFiles).forEach(f -> {
            try {
                InputStream mapStream = new FileInputStream(f.getAbsolutePath());
                InputStream buffer = new BufferedInputStream(mapStream);
                ObjectInput input = new ObjectInputStream(buffer);
                maps.add((Map) input.readObject());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    void draw(GameScreen screen) {
        if (screen != GameScreen.MENU) return;
        StdDraw.setPenColor(StdDraw.WHITE);
        StdDraw.textLeft(16, 1900, "Welcome to oops! : the rhythm game you never asked for");
        if (maps.size() == 0) {
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.filledRectangle(960, 540, 960, 540);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.setPenRadius(0.001);
            StdDraw.setFont(FontManager.standard);
            StdDraw.text(960, 540, "No Maps Found");
            StdDraw.line(760, 495, 1160, 495);
            StdDraw.setFont(FontManager.heading);
            StdDraw.text(960, 460, "please add a map to the maps folder");
            return;
        }
        Map map = maps.get(selectedMap);
        this.player.setSound(map.getSong());
        StdDraw.picture(960, 540, 1920, 1080, map.getImage());
        StdDraw.setPenColor(new Color(0, 0, 0, 50));
        StdDraw.filledRectangle(960, 540, 960, 540);
        StdDraw.setPenColor(StdDraw.WHITE);
        StdDraw.setPenRadius(0.001);
        StdDraw.setFont(FontManager.standard);
        StdDraw.text(960, 540, map.getMapName());
        StdDraw.line(760, 495, 1160, 495);
        StdDraw.setFont(FontManager.heading);
        StdDraw.text(960, 460, map.getMapArtist());
        if(selectedMap != 0) StdDraw.textLeft(30, 540, "<");
        if(selectedMap != maps.size() - 1) StdDraw.textRight(1890, 540, ">");
        StdDraw.text(960, 300, "ENTER to play");
        StdDraw.textLeft(16, 16, "LEFT and RIGHT to switch maps");
    }

    void rightArrow() {
        if(selectedMap == maps.size() - 1) return;
        selectedMap++;
    }

    void leftArrow() {
        if(selectedMap == 0) return;
        selectedMap--;
    }
}
