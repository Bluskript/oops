package game;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;

public class Map implements Serializable {
    private String mapName;
    private String mapArtist;
    private double[] song;
    private ArrayList<Beat> beats = new ArrayList<>();
    transient private BufferedImage image;

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        ImageIO.write(image, "png", out);
    }

    private void readObject(ObjectInputStream in) throws Exception {
        in.defaultReadObject();
        image = ImageIO.read(in);
    }

    public void setBeats(ArrayList<Beat> beats) {
        this.beats = beats;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public void setMapArtist(String mapArtist) {
        this.mapArtist = mapArtist;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    public void setSong(double[] song) {
        this.song = song;
    }

    public ArrayList<Beat> getBeats() {
        return beats;
    }

    public String getMapArtist() {
        return mapArtist;
    }

    public String getMapName() {
        return mapName;
    }

    public BufferedImage getImage() {
        return image;
    }

    public double[] getSong() {
        return song;
    }
}
