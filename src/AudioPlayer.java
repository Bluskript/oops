import StdLib.StdAudio;

public class AudioPlayer implements Runnable {
    private volatile double[] sound = new double[]{};

    @Override
    public void run() {
        while (true) {
            for (int i = 0; i < sound.length; i++) {
                StdAudio.play(sound[i]);
            }
        }
    }

    void setSound(double[] newSound) {
        this.sound = newSound;
    }
}
